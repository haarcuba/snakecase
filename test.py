import snakecase

print (snakecase.convert("YoginthVar"))
#=> yoginth_var

print (snakecase.convert("thisVariable"))
#=> this_variable